class Calc
  def initialize
    @numbers = Array.new
  end

  def get(*args)
    @numbers.push(args).flatten!
  end

  def plus
    res = @numbers.reduce(&:+)
    wipe
    res || 0
  end

  def minus
    - plus
  end

  private

  def wipe
    @numbers = []
  end
end
