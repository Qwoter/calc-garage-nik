#calc_spec.rb
require './lib/calc'

describe Calc do
  before(:each) do
    @c = Calc.new
    @c.get [1, 2], 3
    @c.get [4, [5, 6], 7]
  end

  it "gets parameters" do
    @c.instance_variable_get("@numbers").should eql([1, 2, 3, 4, 5, 6, 7])
  end

  it "sums stack parameters and wipe them" do
    @c.plus.should == 28
    @c.instance_variable_get("@numbers").should == []
  end

  it "subsctract sum of stack from 0" do
    @c.minus.should == -28
    @c.instance_variable_get("@numbers").should == []
  end
end